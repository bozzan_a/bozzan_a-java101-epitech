<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
	
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="resources/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link href="resources/bootstrap-theme.min.css" rel="stylesheet"
	type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Nouveau Cours</title>
</head>
<body>
	<form:form method="post" action="addContact.html">
	<div class="form-group">
	
		<form:label class="form-control" path="module">module</form:label>
		<form:input path="module" />
		<br />
		<form:label class="form-control" path="activity">activity</form:label>
		<form:input path="activity" />
		<br />
		<form:label path="teacher">teacher</form:label>
		<form:select class="form-control" path="teacher" name="listbox">
			<form:option value="value1">label1</form:option>
			<form:option value="value2">label2</form:option>
			<form:option value="value3">label3</form:option>
		</form:select>

		<input type="submit" class="btn btn-default" value="Add Cours" />
	</div>
	</form:form>
</body>
</html>