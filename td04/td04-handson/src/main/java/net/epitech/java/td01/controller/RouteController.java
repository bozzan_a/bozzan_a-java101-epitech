package net.epitech.java.td01.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import net.epitech.java.td01.controller.form.CourseFormElement;
import net.epitech.java.td01.controller.form.TeacherFormElement;
import net.epitech.java.td01.model.Course;
import net.epitech.java.td01.service.contract.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

//je suis une class de type controller dans spring = je peux être injectée et je fais office de servlet.
@Controller
public class RouteController {

	@Autowired
	private Service service;

	@RequestMapping(value = { "/cours", "/" })
	public String getCourses(ModelMap model) throws IOException {

		model.addAttribute("courses", service.getCourses());

		return "courses";
	}

	@RequestMapping(value = "/nouveau-cours")
	public String addNewCourses(ModelMap model) throws IOException {

		model.addAttribute("course", new CourseFormElement());
		return "newcourse";
	}

	@RequestMapping(value = "/nouveau-teacher")
	public String addNewTeachers(ModelMap model) throws IOException {

		model.addAttribute("teacher", new TeacherFormElement());
		return "newteacher";
	}
	
	@RequestMapping(value = "/teacher", method = { RequestMethod.POST })
	public String addNewTeacher(
			@Valid @ModelAttribute("teacher") TeacherFormElement teacher,
			BindingResult result, HttpServletRequest request)
			throws IOException {

		if (result.hasErrors()) {
			return "newteacher";
		}
		
		// TODO: pass those to service layer to create course element
		try {
			service.addTeacher(teacher.getName(), teacher.getEmail());
		} catch (Exception e) {
			result.rejectValue("email", "bademail", "This teacher exists");
			return "newteacher";
		}
		return "redirect:/";
	}
	
	@RequestMapping(value = "/course", method = { RequestMethod.POST })
	public String addNewCourse(
			@Valid @ModelAttribute("course") CourseFormElement course,
			BindingResult result, HttpServletRequest request)
			throws IOException {

		if (result.hasErrors()) {
			return "newcourse";
		}
		// TODO: pass those to service layer to create course element
		course.getName();
		course.getDuration();
		course.getDateStart();
		return "redirect:/";
	}

	@RequestMapping(value = "/test")
	public ModelAndView newCourse(HttpServletResponse response)
			throws IOException {

		return new ModelAndView("test", "command", new Course());
	}
}
